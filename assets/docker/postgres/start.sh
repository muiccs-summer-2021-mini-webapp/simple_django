#!/bin/bash

IMAGE="postgres:13"
CONTAINER_NAME="simple_django_postgres"
RUNNING_PORT="5432"

POSTGRES_USER="simple_django"
POSTGRES_PASSWORD="lIXPQJ%b3^xTb#GL7wE1"
POSTGRES_DB="simple_django"

#============================================================================
# Check if docker container is already existing or running. If so, remove it.
NAME_EXIST="true"
RUNNING=$(docker inspect --format="{{ .State.Running }}" $CONTAINER_NAME 2> /dev/null)
if [ $? -eq 0 ]; then
docker rm -f $CONTAINER_NAME
fi
#============================================================================

docker run -d --name $CONTAINER_NAME \
    -e "POSTGRES_USER=$POSTGRES_USER" \
    -e "POSTGRES_PASSWORD=$POSTGRES_PASSWORD" \
    -e "POSTGRES_DB=$POSTGRES_DB" \
    -e TZ="Asia/Bangkok" \
    -p "$RUNNING_PORT:5432" \
    $IMAGE
