## 01 - Django

### Creating Django project

Open Pycharm and choose new project
![Choose new project](images/01/01-new-project.png)

Start Django server

```bash
$ ./manage.py runserver
```

or just click run in pycharm
![Start server](images/01/02-start-server.png)

Visit http://127.0.0.1:8000/
![Visit Django main page](images/01/03-django-main-page.png)

### Django project structure
```
simple_django/
├── manage.py
├── simple_django/
│   ├── __init__.py
│   ├── asgi.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
```

These files are
- manage.py: command line utility
- Inner simple_django/ is actual Django project.
- simple_django/settings.py: Configuration for Django project.
- simple_django/urls.py: All url declarations for the project will be here.

### Database Setup

By default, SQLite is used as the project database. It is okay to use SQLite if you want to just start Django project 
for prototyping because it is quick, and you don't need to install anything since SQLite is included in python.
However, in real project, it is a best practice to use client/server database such as PostgreSQL, MySQL, SQL Server
, or Oracle. In this project, we are going to use PostgreSQL as our database.

First, let's start postgres container.
```bash
$ docker run -d --name simple_django_postgres \
    -e POSTGRES_USER="simple_django" \
    -e POSTGRES_PASSWORD="lIXPQJ%b3^xTb#GL7wE1" \
    -e POSTGRES_DB="simple_django" \
    -e TZ="Asia/Bangkok" \
    -p 5432:5432 \
    postgres:13
```

After postgres container is up, go to `simple_django/settings.py` and change the value of `DATABASES` variable to this.
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'simple_django',
        'USER': 'simple_django',
        'PASSWORD': 'lIXPQJ%b3^xTb#GL7wE1',
        'HOST': '127.0.0.1',
        'PORT': '5432'
    }
}
```

Then start Django server again. There should be error like this.
![Django server error](images/01/04-django-server-error.png)
The error above means that we're missing database adaptor for postgres. To solve this, we need to install psycopg2
using pipenv.
```bash
$ pipenv install psycopg2-binary # see https://www.psycopg.org/docs/install.html for further explanation
```
After this, out Django server should have no error.

### Quickstart app

Let's create a quickstart app.
```bash
$ ./manage.py startapp quickstart
```

A folder `quickstart` should be created with files as below.
```
simple_django/
├── manage.py
├── quickstart/
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── migrations
│   │   └── __init__.py
│   ├── models.py
│   ├── tests.py
│   └── views.py
```

Create a package name views in `quickstart/`. Then in `quickstart/views/hello_function_based_view.py`, paste the following code.
```python
# quickstart/views/hello_function_based_view.py
from django.http import JsonResponse

def hello(request):
    return JsonResponse({"hello": "world"})
```

To be able to call this view, we need a URLconf for mapping it to URL.
```python
# quickstart/urls.py
from django.urls import path
from quickstart.views import hello_function_based_view

urlpatterns = [
    path('', hello_function_based_view.hello, name='hello'),
]
```
Then we have to register url in quickstart app to our project.
```python
# simple_django/urls.py
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('api/quickstart/', include('quickstart.urls')),
    path('admin/', admin.site.urls),
]
```
The `include()` is a function for including urls from other URLconf module.

Now, let's visit http://localhost:8000/api/quickstart/. You should see json `{"hello": "world"}` in the browser.

### Class-based views vs Function-based views

What we've just created is called Function-based views. There are also another type of view called Class-based views.

Let's create a new view by creating a new file `quickstart/views/hello_class_based_view.py`
```python
from django.http import JsonResponse
from django.views import View

class HelloClassBasedView(View):
    def get(self, request):
        return JsonResponse({"hello": "world"})
```
And in `quickstart/urls.py`
```python
# quickstart/urls.py
from django.urls import path

from quickstart.views import hello_function_based_view
from quickstart.views.hello_class_based_view import HelloClassBasedView

urlpatterns = [
    path('', hello_function_based_view.hello, name='hello'),
    path('hello', HelloClassBasedView.as_view())
]
```
After server is reloaded, go to http://localhost:8000/api/quickstart/hello and you should see the same response as our 
first view.

Further reading: https://www.geeksforgeeks.org/class-based-vs-function-based-views-which-one-is-better-to-use-in-django/

### Model

Let's create our first model. By default, Django has provided us a simple User model, but they recommended us to use a
custom user model since there maybe a chance in the future that we need to customize user model (e.g., using email as
primary key). It is highly recommended adding custom user model as soon as possible when we create Django project
before running any migrations.

```bash
$ ./manage.py startapp user_management # create app called user_management
```
Then, create a python package name `models` inside `user_management/`.
And inside `user_management/models/`, a file `user.py`
```python
# user_management/models/user.py
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    class Meta:
        db_table = "tbl_user"
```

```python
# user_management/models/__init__.py
from user_management.models.user import User
```

Next, we have to tell Django that we want to change from using its default User model to our User model by setting
`AUTH_USER_MODEL` in `simple_django/settings.py`

```python
# simple_django/settings.py
INSTALLED_APPS = [
    ...
    'user_management'
]
...
AUTH_USER_MODEL = 'user_management.User'
```

Up to this point, we did not touch the database yet. The steps to apply the changes that we made to our models into the 
database, it is called `migrations` in Django. It will be done through command line and here are several commands
for migrations.

- **showmigrations**: Display all migrations in the project with the status
- **makemigrations**: Create new migrations script when we made any changes to model
- **sqlmigrate**: Display SQL statement for migration
- **migrate**: Applying or unapplying migrations to the database

Let's run each command.
```bash
$ ./manage.py showmigrations
```
![showmigrations](images/01/05-showmigrations.png)

```bash
./manage.py makemigrations
```
![showmigrations](images/01/06-makemigrations.png)

If you setup git inside the project, you will see that there is new file `user_management/migrations/0001_initial.py` 
created.

```bash
./manage.py sqlmigrate user_management 0001_initial # showing sql equivalent of user_management/migrations/0001_initial.py script
```

Finally
```bash
./manage.py migrate
```

Now we can create a user for development environment.
```bash
$ ./manage.py createsuperuser --username admin --email admin@example.com
```
![createsuperuser](images/01/07-createsuperuser.png)

Let's create two views; one for listing users and the other one for seeing user detail.

```python
# user_management/views/user_list_view.py
from django.core import serializers
from django.http import HttpResponse
from django.views import View

from user_management.models import User

class UserListView(View):
    def get(self, request):
        queryset = User.objects.all()
        json_data = serializers.serialize('json', queryset=queryset)
        return HttpResponse(json_data, content_type='application/json')
```

```python
# user_management/views/user_detail_view.py
from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.views import View

from user_management.models import User

class UserDetailView(View):
    def get(self, request, user_id):
        try:
            user = User.objects.get(pk=user_id)
            return JsonResponse(model_to_dict(user), content_type='application/json')
        except User.DoesNotExist as e:
            return JsonResponse({"error": "User not found"}, status=404)
```

```python
# user_management/views/__init__.py
from user_management.views.user_list_view import UserListView
from user_management.views.user_detail_view import UserDetailView
```

```python
# user_management/urls.py
from django.urls import path
from user_management.views import UserListView, UserDetailView

urlpatterns = [
    path('list', UserListView.as_view()),
    path('detail/<int:user_id>', UserDetailView.as_view())
]
```
```python
# simple_django/urls.py
urlpatterns = [
    ...
    path('api/users/', include('user_management.urls')),
    ...
]
```

Then go to http://localhost:8000/api/users/list and http://localhost:8000/api/users/detail/1 (you can change to any number).
