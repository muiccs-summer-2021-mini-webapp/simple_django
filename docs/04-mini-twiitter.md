## 04 - Mini Twiitter

### Tweeet

```bash
$ ./manage.py startapp mini_twiitter
```

```python
# mini_twiitter/models/base_model.py
from django.db import models

class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
```

```python
# mini_twiitter/models/tweeet.py
from django.conf import settings
from django.db import models

from mini_twiitter.models.base_model import BaseModel


class Tweeet(BaseModel):
    message = models.TextField(blank=False)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        db_table = "tbl_tweeet"
```

```python
# mini_twiitter/models/__init__.py
from mini_twiitter.models.tweeet import Tweeet
```

```python
# settings.py
INSTALLED_APPS = [
    ...
    'mini_twiitter'
]
```

```bash
$ ./manage.py makemigrations
$ ./manage.py migrate
```

```python
# mini_twiitter/serializers/tweeet_serializer.py
from rest_framework import serializers

from mini_twiitter.models import Tweeet


class TweeetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tweeet
        fields = '__all__'
        read_only_fields = [
            "id", "author", "created_at", "modified_at"
        ]
```

```python
# mini_twiitter/views/tweeet_view_set.py
from rest_framework import viewsets
from mini_twiitter.models import Tweeet
from mini_twiitter.serializers.tweeet_serializer import TweeetSerializer

class TweeetViewSet(viewsets.ModelViewSet):
    queryset = Tweeet.objects.all()
    serializer_class = TweeetSerializer

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
```

```python
# mini_twiitter/urls.py
from django.urls import path, include
from rest_framework import routers

from mini_twiitter.views.tweeet_view_set import TweeetViewSet

router = routers.DefaultRouter()
router.register('tweeets', TweeetViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
```

```python
# simple_django/urls.py
...
urlpatterns = [
    ...
    path('api/mini-twiitter/', include('mini_twiitter.urls')),
    ...
]
```

### Replied Tweeet

```python
# mini_twiitter/models/replied_tweeet.py
from django.conf import settings
from django.db import models

from mini_twiitter.models import Tweeet
from mini_twiitter.models.base_model import BaseModel

class RepliedTweeet(BaseModel):
    message = models.TextField(blank=False)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    tweeet = models.ForeignKey(Tweeet, on_delete=models.CASCADE)

    class Meta:
        db_table = "tbl_replied_tweeet"
```

```python
# mini_twiitter/models/__init__.py
...
from mini_twiitter.models.replied_tweeet import RepliedTweeet
```

```bash
$ ./manage.py makemigrations
$ ./manage.py migrate
```

```python
# mini_twiitter/serializers/replied_tweeet_serializer.py
from rest_framework import serializers
from mini_twiitter.models import RepliedTweeet

class RepliedTweeetSerializer(serializers.ModelSerializer):
    class Meta:
        model = RepliedTweeet
        fields = '__all__'
        read_only_fields = [
            "id", "author", "tweeet", "created_at", "modified_at"
        ]
```

```python
# mini_twiitter/views/replied_tweeet_view_set.py
from rest_framework import viewsets, mixins
from mini_twiitter.models import RepliedTweeet
from mini_twiitter.serializers.replied_tweeet_serializer import RepliedTweeetSerializer

class RepliedTweeetViewSet(viewsets.GenericViewSet,
                           mixins.RetrieveModelMixin,
                           mixins.CreateModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin):
    queryset = RepliedTweeet.objects.all()
    serializer_class = RepliedTweeetSerializer

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
```

```python
# mini_twiitter/urls.py
from django.urls import path, include
from rest_framework import routers

from mini_twiitter.views.replied_tweeet_view_set import RepliedTweeetViewSet
from mini_twiitter.views.tweeet_view_set import TweeetViewSet

router = routers.DefaultRouter()
router.register('tweeets', TweeetViewSet)
router.register('replied-tweeets', RepliedTweeetViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
```

### Modifying detail tweeet

```python
# mini_twiitter/models/replied_tweeet.py
...
class RepliedTweeet(BaseModel):
    ...
    tweeet = models.ForeignKey(Tweeet, on_delete=models.CASCADE, related_name="replied_tweeets")
    ...
```

```bash
$ ./manage.py makemigrations
$ ./manage.py migrate
```

```python
# mini_twiitter/serializers/tweeet_detail_serializer.py
from rest_framework import serializers

from mini_twiitter.models import Tweeet
from mini_twiitter.serializers.replied_tweeet_serializer import RepliedTweeetSerializer

class TweeetDetailSerializer(serializers.ModelSerializer):
    replied_tweeets = RepliedTweeetSerializer(many=True)

    class Meta:
        model = Tweeet
        fields = '__all__'
        read_only_fields = ["id", "author", "created_at", "modified_at", "replied_tweeets"]
```

```python
# mini_twiitter/views/tweeet_view_set.py
from rest_framework import viewsets
from mini_twiitter.models import Tweeet
from mini_twiitter.serializers.tweeet_detail_serializer import TweeetDetailSerializer
from mini_twiitter.serializers.tweeet_serializer import TweeetSerializer

class TweeetViewSet(viewsets.ModelViewSet):
    queryset = Tweeet.objects.all()
    serializer_class = TweeetSerializer
    serializer_classes = {
        'list': TweeetSerializer,
        'retrieve': TweeetDetailSerializer,
        'create': TweeetSerializer,
        'update': TweeetSerializer,
        'partial_update': TweeetSerializer,
        'destroy': TweeetSerializer
    }

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action)
```
