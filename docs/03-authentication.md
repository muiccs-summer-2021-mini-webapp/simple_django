## 03 - Authentication

```bash
$ pipenv install dj-rest-auth
```

In `settings.py`
```python
INSTALLED_APPS = (
    ...,
    'rest_framework',
    'rest_framework.authtoken',
    ...,
    'dj_rest_auth'
)
```

In **urls.py**
```python
urlpatterns = [
    ...,
    path('dj-rest-auth/', include('dj_rest_auth.urls'))
]
```

```bash
$ ./manage.py migrate
```