## 05 - Django Admin

```python
# user_management/admin.py
from django.contrib import admin

# Register your models here.
from user_management.models import User

admin.site.register(User)
```

```python
# mini_twiitter/admin.py
from django.contrib import admin

# Register your models here.
from mini_twiitter.models import Tweeet, RepliedTweeet

admin.site.register(Tweeet)
admin.site.register(RepliedTweeet)
```

Then visit http://localhost:8000/admin
