## 02 - Django REST Framework

So far, both UserListView and UserDetailView looks ok. However, imagine if we have 10K of users, we may want to modify
UserListView to support pagination and filtering. Remember, there are more than 1 model in real application which means
we will have much work to do. This is where Django REST Framework comes to rescue us.

### Installing
```bash
pipenv install djangorestframework
pipenv install markdown       # Markdown support for the browsable API.
pipenv install django-filter  # Filtering support
```

Add 'rest_framework' to your INSTALLED_APPS setting.

```python
INSTALLED_APPS = [
    ...
    'rest_framework',
]
```

Add this in `simple_django/urls.py` as well.
```python
urlpatterns = [
    ...
    path('api-auth/', include('rest_framework.urls'))
]
```

It's nice to have swagger in our project for development environment.

```bash
$ pipenv install drf-yasg
```

```python
# simple_django/settings.py
INSTALLED_APPS = [
   ...
   'django.contrib.staticfiles',  # required for serving swagger ui's css/js files
   'drf_yasg',
   ...
]
```

```python
# simple_django/urls.py
...
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

...

schema_view = get_schema_view(
   openapi.Info(
      title="simple_django API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

urlpatterns = [
   url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
   url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
   url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
   ...
]
```

Now we have swagger in our project. Go to http://localhost:8000/swagger/

### Quick CRUD for user
First, we need to create a serializer which responsible for converting model instances into native python data types and then to JSON, XML or other content types.
```python
# user_management/serializers/user_serializer.py
from rest_framework import serializers
from user_management.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id", "last_login", "is_superuser", "username", "first_name", "last_name", "email", "is_staff",
            "is_active", "date_joined"
        ]
        read_only_fields = [
            "id", "last_login", "date_joined"
        ]
```
In this example, we use ModelSerializer for our UserSerializer. ModelSerializer provides shortcut for mapping Model 
fields to Serializer.

```bash
$ ./manage.py shell
```

```python
Python 3.8.0
>>> from user_management.serializers.user_serializer import UserSerializer
>>> from user_management.models.user import User
>>> user = User(email="test@example.com", username="foobar", first_name="foo", last_name="bar")
>>> serializer = UserSerializer(user)
>>> serializer.data
# {'id': None, 'last_login': None, 'is_superuser': False, 'username': 'foobar', 'first_name': 'foo', 'last_name': 'bar', 'email': 'test@example.com', 'is_staff': False, 'is_active': True, 'date_joined': '2021-08-12T19:33:33.381530Z'}
>>> import json
>>> json.dumps(serializer.data)
# '{"id": null, "last_login": null, "is_superuser": false, "username": "foobar", "first_name": "foo", "last_name": "bar", "email": "test@example.com", "is_staff": false, "is_active": true, "date_joined": "2021-08-12T19:33:33.381530Z"}'
```

Next, let's add view.
```python
# user_management/views/user_view_set.py
from rest_framework import viewsets
from user_management.models import User
from user_management.serializers.user_serializer import UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
```

```python
# user_management/views/__init__.py
...
from user_management.views.user_view_set import UserViewSet
```

```python
# user_management/urls.py
from django.urls import path, include
from rest_framework import routers
from user_management.views import UserListView, UserDetailView, UserViewSet

router = routers.DefaultRouter() 
router.register('', UserViewSet) # Register all of the common routes given resourceful view
# For example,
# GET /users
# GET /users/:id
# POST /users
# PUT /users/:id
# PATCH /users/:id
# DELETE /users/:id

urlpatterns = [
    path('', include(router.urls)),
    path('list', UserListView.as_view()),
    path('detail/<int:user_id>', UserDetailView.as_view())
]
```

### Pagination and Filtering

The quickest way to add pagination feature in the project is to add the following line in **settings**.

```python
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 100
}
```

For filtering.
```python
# user_management/views/user_view_set.py
from rest_framework import viewsets, filters
from user_management.models import User
from user_management.serializers.user_serializer import UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['username', 'email']
    ordering_fields = ['username', 'email']
    ordering = ['username'] # default ordering field
```
