## simple_django

### Table of Contents
1. [Intro to Django](./docs/01-django.md)
2. [Django REST Framework](./docs/02-drf.md)
3. [Authentication](./docs/03-authentication.md)
4. [Mini Twiitter](./docs/04-mini-twiitter.md)
5. [Django Admin](./docs/05-django-admin.md)
