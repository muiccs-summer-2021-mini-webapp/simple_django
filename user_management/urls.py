from django.urls import path, include
from rest_framework import routers

from user_management.views import UserListView, UserDetailView, UserViewSet

router = routers.DefaultRouter()
router.register('', UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('list', UserListView.as_view()),
    path('detail/<int:user_id>', UserDetailView.as_view())
]
