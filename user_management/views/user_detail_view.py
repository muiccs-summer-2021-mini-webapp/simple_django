from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.views import View

from user_management.models import User


class UserDetailView(View):
    def get(self, request, user_id):
        try:
            user = User.objects.get(pk=user_id)
            return JsonResponse(model_to_dict(user), content_type='application/json')
        except User.DoesNotExist as e:
            return JsonResponse({"error": "User not found"}, status=404)
