from django.core import serializers
from django.http import HttpResponse
from django.views import View

from user_management.models import User


class UserListView(View):
    def get(self, request):
        queryset = User.objects.all()
        json_data = serializers.serialize('json', queryset=queryset)
        return HttpResponse(json_data, content_type='application/json')
