from rest_framework import serializers

from user_management.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id", "last_login", "is_superuser", "username", "first_name", "last_name", "email", "is_staff",
            "is_active", "date_joined"
        ]
        read_only_fields = [
            "id", "last_login", "date_joined"
        ]
