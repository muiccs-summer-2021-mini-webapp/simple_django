from django.http import JsonResponse
from django.views import View


class HelloClassBasedView(View):
    def get(self, request):
        return JsonResponse({"hello": "world"})
