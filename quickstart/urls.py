from django.urls import path

from quickstart.views import hello_function_based_view
from quickstart.views.hello_class_based_view import HelloClassBasedView

urlpatterns = [
    path('', hello_function_based_view.hello, name='hello'),
    path('hello', HelloClassBasedView.as_view())
]
