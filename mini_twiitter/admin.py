from django.contrib import admin

# Register your models here.
from mini_twiitter.models import Tweeet, RepliedTweeet

admin.site.register(Tweeet)
admin.site.register(RepliedTweeet)
