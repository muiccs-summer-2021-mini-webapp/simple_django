from django.urls import path, include
from rest_framework import routers

from mini_twiitter.views.replied_tweeet_view_set import RepliedTweeetViewSet
from mini_twiitter.views.tweeet_view_set import TweeetViewSet

router = routers.DefaultRouter()
router.register('tweeets', TweeetViewSet)
router.register('replied-tweeets', RepliedTweeetViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
