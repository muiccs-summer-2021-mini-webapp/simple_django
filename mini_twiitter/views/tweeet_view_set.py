from rest_framework import viewsets

from mini_twiitter.models import Tweeet
from mini_twiitter.serializers.tweeet_detail_serializer import TweeetDetailSerializer
from mini_twiitter.serializers.tweeet_serializer import TweeetSerializer


class TweeetViewSet(viewsets.ModelViewSet):
    queryset = Tweeet.objects.all()
    serializer_class = TweeetSerializer
    serializer_classes = {
        'list': TweeetSerializer,
        'retrieve': TweeetDetailSerializer,
        'create': TweeetSerializer,
        'update': TweeetSerializer,
        'partial_update': TweeetSerializer,
        'destroy': TweeetSerializer
    }

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action)
