from rest_framework import viewsets, mixins

from mini_twiitter.models import RepliedTweeet
from mini_twiitter.serializers.replied_tweeet_serializer import RepliedTweeetSerializer


class RepliedTweeetViewSet(viewsets.GenericViewSet,
                           mixins.RetrieveModelMixin,
                           mixins.CreateModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin):
    queryset = RepliedTweeet.objects.all()
    serializer_class = RepliedTweeetSerializer

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
