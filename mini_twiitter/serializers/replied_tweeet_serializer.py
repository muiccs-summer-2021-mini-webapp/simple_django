from rest_framework import serializers

from mini_twiitter.models import RepliedTweeet


class RepliedTweeetSerializer(serializers.ModelSerializer):
    class Meta:
        model = RepliedTweeet
        fields = '__all__'
        read_only_fields = [
            "id", "author", "created_at", "modified_at"
        ]
