from rest_framework import serializers

from mini_twiitter.models import Tweeet


class TweeetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tweeet
        fields = '__all__'
        read_only_fields = [
            "id", "author", "created_at", "modified_at"
        ]
