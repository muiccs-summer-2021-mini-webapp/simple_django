from rest_framework import serializers

from mini_twiitter.models import Tweeet
from mini_twiitter.serializers.replied_tweeet_serializer import RepliedTweeetSerializer


class TweeetDetailSerializer(serializers.ModelSerializer):
    replied_tweeets = RepliedTweeetSerializer(many=True)

    class Meta:
        model = Tweeet
        fields = '__all__'
        read_only_fields = ["id", "author", "created_at", "modified_at", "replied_tweeets"]
