from django.conf import settings
from django.db import models

from mini_twiitter.models.base_model import BaseModel


class Tweeet(BaseModel):
    message = models.TextField(blank=False)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        db_table = "tbl_tweeet"
