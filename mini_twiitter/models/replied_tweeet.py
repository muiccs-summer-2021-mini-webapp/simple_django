from django.conf import settings
from django.db import models

from mini_twiitter.models import Tweeet
from mini_twiitter.models.base_model import BaseModel


class RepliedTweeet(BaseModel):
    message = models.TextField(blank=False)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    tweeet = models.ForeignKey(Tweeet, on_delete=models.CASCADE, related_name="replied_tweeets")

    class Meta:
        db_table = "tbl_replied_tweeet"
